#!/bin/sh
################################################################
#    @name : favicon-resizer                                 #
#    @description : generate a wide range of favicons from     #
#                  a source image (should be min 196x196)      #
#    @param :                                                  #
#        $1 : source image path                                #
#                                                              #
# Repository : https://gitlab.com/jd-si/favicon-resizer      #
#                                                              #
# Require : mogrify command available on your system           #
################################################################

# Generated sizes
SIZES="196 192 168 152 144 120 114 96 76 72 64 60 48 32 16"

# Colors
RED='\033[0;31m'
BLUE='\033[0;34m'
GRAY='\033[1;30m'
NC='\033[0m'

################################################################
#    function : info                                           #
#    description : Log info message                            #
#    params :                                                  #
#        $1 : Log message                                      #
################################################################
info() {
	TIME=`date +"%Y-%m-%d %T"`

	# Echo to stdout
	echo "[${GRAY}${TIME}${NC}] [${BLUE}INFO${NC}] $1"
}

################################################################
#    function : err                                            #
#    description : Log error message to stderr and exit if     #
#                  exit code is provided                       #
#    params :                                                  #
#        $1 : Log message                                      #
#        $2 : Exit code                                        #
################################################################
err() {
	TIME=`date +"%Y-%m-%d %T"`
	echo "[${GRAY}${TIME}${NC}] [${RED}ERROR${NC}] $1." 1>&2
	# Exit if error code is set
	[ -n "$2" ] && exit $2
}


[ "$#" -ne 1 ] && err "Usage : $0 <SOURCE_IMAGE>" 1
# Check if input file exists
[ ! -f "$1" ] && err "File $1 does not exists" 2

# Check if mogrify command is available
which mogrify
[ "$?" -ne 0 ] && err "Mogrify must be installed. Please install imagemagick using your package manager  (eg : apt-get install imagemagick)" 3

FILE_IN=$(basename -- "$1")
EXT="${FILE_IN##*.}"
FILENAME="${FILE_IN%.*}"

for i in $SIZES; do
	info "Copy $FILE_IN to $FILENAME-$i.$EXT"
	cp "$FILE_IN" "$FILENAME-$i.$EXT"
	info "Resize $FILENAME-$i.$EXT to ${i}x${i}"
	mogrify -resize ${i}x${i} "$FILENAME-$i.$EXT"
done
