# Favicon resizer

A small shell script that provides generation of multiple sizes of favicons from a jpg/png image.

Generated images have 1:1 aspect ratio and following sizes.

* 196px
* 192px
* 168px
* 152px
* 144px
* 120px
* 114px
* 96px
* 76px
* 72px
* 64px
* 60px
* 48px
* 32px
* 16px


## Requirements
Mogrify must be installed on your system. Use your package manager to install it.
